---
- mr.2062
- mr.2079
---

all: Add support for the new `xrt_system` and `xrt_session` objects.
